# Quote Finder API

Application Made with Symfony

## Installation 

### Requirements

[Docker](https://docs.docker.com/engine/install/)

[Docker compose](https://docs.docker.com/compose/install/)

### Instruction 
Open a terminal and execute
```bash
./first_start_from_host.sh
```
 
This command will build a Docker image for PHP and pull images for MySQL and NGINX.

The nginx is running on port 1000

## Usage

### Normal Access
API Crud
##### GET Quote by Author
###### Method: GET
http://localhost:1000/quotes/{author}

``
author string (text slugify)
``

```
Headers
Accept: application/json
```
```
Query Params
limit: int, values min 1 and max 10
```
http://localhost:1000/quotes/steve-jobs?limit=2

###### Response:
```
[
    "THE ONLY WAY TO DO GREAT WORK IS TO LOVE WHAT YOU DO!",
    "YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!"
]
```

### Run Functional Test
After run ./first_start_from_host.sh
execute
```bash
./run_test.sh
```
Will download PHP Unit and run the functional test