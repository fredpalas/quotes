<?php

namespace App\Tests\Controller;

use Hautelook\AliceBundle\PhpUnit\RecreateDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use function json_decode;

class GetQuotesTest extends WebTestCase
{
    use RecreateDatabaseTrait;

    private const ENDPOINT = '/quotes/%s';

    /** @var KernelBrowser|null  */
    protected static $baseClient = null;

    public function setUp(): void
    {
        parent::setUp();

        if (null === self::$baseClient) {
            self::$baseClient = static::createClient();
            self::$baseClient->setServerParameters([
                'CONTENT_TYPE' => 'application/json',
                'HTTP_ACCEPT' => 'application/json',
            ]);
        }
    }

    public function testRegisterAction(): void
    {
        $payload = [
            'limit' => 10
        ];

        self::$baseClient->request(
            Request::METHOD_GET,
            sprintf(self::ENDPOINT,'steve-jobs'),
            $payload
        );

        $response = self::$baseClient->getResponse();

        self::assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        self::assertIsArray($responseData);
        self::assertContains("THE ONLY WAY TO DO GREAT WORK IS TO LOVE WHAT YOU DO!", $responseData);
        self::assertContains("YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!", $responseData);
        self::assertCount(2, $responseData);
    }

    public function testRegisterActionWithInvalidLimit(): void
    {
        $payload = [
            'limit' => 11
        ];

        self::$baseClient->request(
            Request::METHOD_GET,
            sprintf(self::ENDPOINT,'steve-jobs'),
            $payload
        );

        $response = self::$baseClient->getResponse();

        self::assertEquals(JsonResponse::HTTP_BAD_REQUEST, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        self::assertArrayHasKey('message', $responseData);
        self::assertIsString($responseData['message']);
        self::assertStringContainsString("This value should be between 1 and 10.", $responseData['message']);
    }

    public function testRegisterActionWithInvalidAuthor(): void
    {
        $payload = [
            'limit' => 10
        ];

        self::$baseClient->request(
            Request::METHOD_GET,
            sprintf(self::ENDPOINT,'steve-jobss'),
            $payload
        );

        $response = self::$baseClient->getResponse();

        self::assertEquals(JsonResponse::HTTP_NOT_FOUND, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        self::assertArrayHasKey('message', $responseData);
        self::assertIsString($responseData['message']);
        self::assertStringContainsString("Data for the type 'quotes' was not found", $responseData['message']);
    }
}
