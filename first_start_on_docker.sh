# bin/bash
composer install
bin/console doctrine:database:create -n
bin/console doctrine:migrations:migrate -n
bin/console doctrine:fixture:load -n
bin/console doctrine:database:create -n --env=test
bin/console doctrine:migrations:migrate -n --env=test