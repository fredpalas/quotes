<?php

namespace App\Entity;

use Ramsey\Uuid\Uuid;

class Quote
{
    /** @var string  */
    private $id;
    /** @var string  */
    private $quote;
    /** @var string  */
    private $author;

    public function __construct(string $id, string $quote, string $author) {

        $this->id = $id;
        $this->quote = $quote;
        $this->author = $author;
    }

    public static function create(string $quote, string $author): self
    {
        return new self(Uuid::uuid4()->toString(), $quote, $author);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getQuote(): string
    {
        return $this->quote;
    }

    /**
     * @param string $quote
     */
    public function setQuote(string $quote): void
    {
        $this->quote = $quote;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }
}
