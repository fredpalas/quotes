<?php


namespace App\Service;


use App\Entity\Quote;
use App\Exception\DataNotFoundException;
use App\Repository\QuoteRepository;

class GetQuoteService
{
    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    public function __construct(QuoteRepository $quoteRepository) {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param string $author
     * @param int $limit
     * @return array
     * @throws DataNotFoundException
     */
    public function __invoke(string $author, int $limit = 10): array
    {
        $results = $this->quoteRepository->getAllQuotesByAuthor($author, $limit);

        if (!$results || count($results) === 0) {
            throw DataNotFoundException::fromClassNameAndIdentifier('quotes');
        }

        foreach ($results as &$result) {
            $last = substr($result, -1);
            if (in_array($last, ['"','.','!','?',"'",])) {
                $result = substr_replace($result ,"!",-1);
                continue;
            }
            $result = $result . '!';
        }

        return $results;
    }
}