<?php

namespace App\Http;

use App\Http\DTO\RequestDTO;
use App\Http\Transformer\RequestBodyTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestValueResolver implements ArgumentValueResolverInterface
{
    /**
     * @var RequestBodyTransformer
     */
    private $requestBodyTransformer;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        RequestBodyTransformer $requestBodyTransformer,
        ValidatorInterface $validator
    ) {
        $this->requestBodyTransformer = $requestBodyTransformer;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        if (!class_exists($argument->getType())) {
            return false;
        }
        $reflectionClass = new \ReflectionClass($argument->getType());
        if ($reflectionClass->implementsInterface(RequestDTO::class)) {
            return true;
        }

        return false;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $this->requestBodyTransformer->transform($request);

        $class = $argument->getType();
        $dto = new $class($request);

        $errors = $this->validator->validate($dto);

        if (\count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }

        yield $dto;
    }
}