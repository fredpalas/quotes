<?php


namespace App\Exception;


use Doctrine\ORM\EntityNotFoundException;
use Throwable;

class DataNotFoundException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public static function fromClassNameAndIdentifier($className)
    {

        return new self(
            'Data for the type \'' . $className . '\'' . ' was not found'
        );
    }
}