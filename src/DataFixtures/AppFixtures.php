<?php

namespace App\DataFixtures;

use App\Entity\Quote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\SplFileInfo;

class AppFixtures extends Fixture
{

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag) {

        $this->parameterBag = $parameterBag;
    }

    public function load(ObjectManager $manager): void
    {
        $dir = $this->parameterBag->get('kernel.project_dir');
        $file = new SplFileInfo(sprintf('%s/config/resources/quotes.json', $dir), '', '');
        $jsonContent = $file->getContents();
        $data = json_decode($jsonContent, true);
        foreach ($data['quotes'] as $quote) {
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $quote['author'])));
            $quote = Quote::create($quote['quote'], $slug);
            $manager->persist($quote);
        }


        $manager->flush();
    }
}
