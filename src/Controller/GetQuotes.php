<?php


namespace App\Controller;


use App\Exception\DataNotFoundException;
use App\Http\DTO\QuoteRequest;
use App\Service\GetQuoteService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetQuotes
{
    /**
     * @var GetQuoteService
     */
    private $getQuoteService;

    public function __construct(GetQuoteService $getQuoteService)
    {
        $this->getQuoteService = $getQuoteService;
    }

    public function __invoke(QuoteRequest $request, $author): JsonResponse
    {
        try {
            $quotes = $this->getQuoteService->__invoke($author, $request->getLimit());
        } catch (DataNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }

        return new JsonResponse($quotes);
    }
}